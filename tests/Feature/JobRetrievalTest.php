<?php

namespace Laravel\Horizon\Tests\Feature;

use Cake\Chronos\Chronos;
use Laravel\Horizon\JobPayload;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\Facades\Redis;
use Laravel\Horizon\Tests\IntegrationTest;
use Laravel\Horizon\Contracts\JobRepository;

class JobRetrievalTest extends IntegrationTest
{
    public function test_pending_jobs_can_be_retrieved()
    {
        $ids = [];

        $ids[] = Queue::push(new Jobs\BasicJob);
        $ids[] = Queue::push(new Jobs\BasicJob);
        $ids[] = Queue::push(new Jobs\BasicJob);
        $ids[] = Queue::push(new Jobs\BasicJob);
        $ids[] = Queue::push(new Jobs\BasicJob);

        $repository = resolve(JobRepository::class);

        $pending = $repository->getPending();

        // Test getting all jobs...
        $this->assertEquals(5, count($pending));
        $this->assertEquals($ids[0], $pending->first()->id);
        $this->assertEquals(Jobs\BasicJob::class, $pending->first()->name);
        $this->assertEquals(0, $pending->first()->index);
        $this->assertEquals($ids[4], $pending->last()->id);
        $this->assertEquals(4, $pending->last()->index);

        // Test pagination...
        $pending = $repository->getPending(1);
        $this->assertEquals(3, count($pending));
        $this->assertEquals($ids[2], $pending->first()->id);
        $this->assertEquals(2, $pending->first()->index);
        $this->assertEquals($ids[4], $pending->last()->id);
        $this->assertEquals(4, $pending->last()->index);

        // Test no results...
        $pending = $repository->getPending(4);
        $this->assertEquals(0, count($pending));

        // Get recent jobs...
        $this->work();
        $this->work();

        $recent = $repository->getRecent();

        $this->assertEquals(5, count($recent));
        $this->assertEquals($ids[4], $recent->first()->id);
        $this->assertEquals(0, $recent->first()->index);
        $this->assertEquals($ids[0], $recent->last()->id);
        $this->assertEquals(4, $recent->last()->index);
        $this->assertEquals('completed', $recent->last()->status);

        // Paginate recent jobs...
        $recent = $repository->getRecent(1);

        $this->assertEquals(3, count($recent));
        $this->assertEquals($ids[2], $recent->first()->id);
        $this->assertEquals(2, $recent->first()->index);
        $this->assertEquals($ids[0], $recent->last()->id);
        $this->assertEquals(4, $recent->last()->index);
    }


    public function test_recent_jobs_are_correctly_trimmed_and_expired()
    {
        $ids = [];

        $ids[] = Queue::push(new Jobs\BasicJob);
        $ids[] = Queue::push(new Jobs\BasicJob);
        $ids[] = Queue::push(new Jobs\BasicJob);
        $ids[] = Queue::push(new Jobs\BasicJob);
        $ids[] = Queue::push(new Jobs\BasicJob);

        $repository = resolve(JobRepository::class);
        Chronos::setTestNow(Chronos::now()->addHours(3));

        $this->assertEquals(5, Redis::connection('horizon-jobs')->zcard('recent_jobs'));

        $repository->trimRecentJobs();
        $this->assertEquals(0, Redis::connection('horizon-jobs')->zcard('recent_jobs'));

        // Assert job record has a TTL...
        $repository->completed(new JobPayload(json_encode(['id' => $ids[0]])));
        $this->assertTrue(Redis::connection('horizon-jobs')->ttl($ids[0]) > 0);

        Chronos::setTestNow();
    }


    public function test_paginating_large_job_results_gives_correct_amounts()
    {
        $ids = [];

        for ($i = 0; $i < 75; $i++) {
            $ids[] = Queue::push(new Jobs\BasicJob);
        }

        $repository = resolve(JobRepository::class);

        $pending = $repository->getPending();
        $this->assertEquals(50, count($pending));

        $pending = $repository->getPending($pending->last()->index);
        $this->assertEquals(25, count($pending));
    }
}
