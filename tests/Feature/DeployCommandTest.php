<?php

namespace Laravel\Horizon\Tests\Feature;

use Mockery;
use Laravel\Horizon\Tests\IntegrationTest;
use Laravel\Horizon\Console\Actions\Deploy;
use Laravel\Horizon\Contracts\MasterSupervisorRepository;

class DeployCommandTest extends IntegrationTest
{
    public function test_machine_is_reprovisioned()
    {
        $command = Mockery::mock('StdClass');
        $command->shouldIgnoreMissing();

        $masterRepo = Mockery::mock(MasterSupervisorRepository::class);
        $masterRepo->shouldReceive('names')->andReturn([gethostname()]);
        $this->app->instance(MasterSupervisorRepository::class, $masterRepo);

        $command->shouldReceive('call')->once()->with('horizon:stop');
        $command->shouldReceive('call')->once()->with('horizon:start');
        $command->shouldReceive('call')->once()->with('horizon:provision');
        $command->shouldReceive('call')->once()->with('horizon:continue', ['machine' => gethostname()]);

        $command->shouldReceive('getLaravel->environment')->andReturn('production');

        $action = new Deploy($command);
        $action->handle();
    }


    public function test_machine_may_wait_to_start()
    {
        $command = Mockery::mock('StdClass');
        $command->shouldIgnoreMissing();

        $masterRepo = Mockery::mock(MasterSupervisorRepository::class);
        $masterRepo->shouldReceive('names')->andReturn([gethostname()]);
        $this->app->instance(MasterSupervisorRepository::class, $masterRepo);

        $command->shouldReceive('call')->once()->with('horizon:stop');
        $command->shouldReceive('call')->once()->with('horizon:start');
        $command->shouldReceive('call')->once()->with('horizon:provision');
        $command->shouldReceive('call')->never()->with('horizon:continue', ['machine' => gethostname()]);

        $command->shouldReceive('argument')->with('cluster')->andReturn('local');
        $command->shouldReceive('option')->with('wait')->andReturn(true);

        $action = new Deploy($command);
        $action->handle();
    }
}
