<?php

namespace Laravel\Horizon\Tests\Feature;

use Mockery;
use Laravel\Horizon\Lock;
use Laravel\Horizon\Tests\IntegrationTest;
use Laravel\Horizon\Console\Actions\Revive;
use Laravel\Horizon\Contracts\MasterSupervisorRepository;

class ReviveCommandTest extends IntegrationTest
{
    public function test_deploy_is_not_run_if_no_masters_are_dead()
    {
        $command = Mockery::mock('StdClass');
        $command->shouldIgnoreMissing();

        $repository = Mockery::mock(MasterSupervisorRepository::class);
        $repository->shouldReceive('names')->andReturn([gethostname()]);
        $this->app->instance(MasterSupervisorRepository::class, $repository);

        $command->shouldReceive('getLaravel->environment')->andReturn('production');
        $command->shouldReceive('call')->never();

        $action = new Revive($command);
        $action->handle();
    }


    public function test_deploy_is_not_run_if_currently_deploying()
    {
        $command = Mockery::mock('StdClass');
        $command->shouldIgnoreMissing();

        $repository = Mockery::mock(MasterSupervisorRepository::class);
        $repository->shouldReceive('names')->andReturn([]);
        $this->app->instance(MasterSupervisorRepository::class, $repository);

        resolve(Lock::class)->get(gethostname().':deploying');

        $command->shouldReceive('getLaravel->environment')->andReturn('production');
        $command->shouldReceive('call')->never();

        $action = new Revive($command);
        $action->handle();
    }


    public function test_deploy_is_run_when_masters_have_died()
    {
        $command = Mockery::mock('StdClass');
        $command->shouldIgnoreMissing();

        $repository = Mockery::mock(MasterSupervisorRepository::class);
        $repository->shouldReceive('names')->andReturn([]);
        $this->app->instance(MasterSupervisorRepository::class, $repository);

        $command->shouldReceive('getLaravel->environment')->andReturn('production');
        $command->shouldReceive('call')->with('horizon:deploy', ['cluster' => 'production']);

        $action = new Revive($command);
        $action->handle();
    }


    public function test_cluster_can_be_specified()
    {
        $command = Mockery::mock('StdClass');
        $command->shouldIgnoreMissing();

        $repository = Mockery::mock(MasterSupervisorRepository::class);
        $repository->shouldReceive('names')->andReturn([]);
        $this->app->instance(MasterSupervisorRepository::class, $repository);

        $command->shouldReceive('argument')->with('cluster')->andReturn('local');
        $command->shouldReceive('call')->with('horizon:deploy', ['cluster' => 'local']);

        $action = new Revive($command);
        $action->handle();
    }
}
