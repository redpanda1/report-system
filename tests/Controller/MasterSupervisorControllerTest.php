<?php

namespace Laravel\Horizon\Tests\Controller;

use Laravel\Horizon\Horizon;
use Laravel\Horizon\Supervisor;
use Laravel\Horizon\MasterSupervisor;
use Laravel\Horizon\SupervisorOptions;
use Laravel\Horizon\Tests\IntegrationTest;
use Laravel\Horizon\Contracts\SupervisorRepository;
use Laravel\Horizon\Contracts\MasterSupervisorRepository;

class MasterSupervisorControllerTest extends IntegrationTest
{
    public function test_master_supervisor_listing_without_supervisors()
    {
        Horizon::auth(function () {
            return true;
        });
        resolve(MasterSupervisorRepository::class)->update(new MasterSupervisor);

        $master = new MasterSupervisor;
        $master->name = 'risa-2';
        resolve(MasterSupervisorRepository::class)->update($master);

        $response = $this->actingAs(new Fakes\User)->get('/horizon/api/masters');

        $response->assertJson([
            gethostname() => ['name' => gethostname(), 'status' => 'paused'],
            'risa-2' => ['name' => 'risa-2', 'status' => 'paused']
        ]);
    }


    public function test_master_supervisor_listing_with_supervisors()
    {
        Horizon::auth(function () {
            return true;
        });
        resolve(MasterSupervisorRepository::class)->update(new MasterSupervisor);

        $master = new MasterSupervisor;
        $master->name = 'risa-2';
        resolve(MasterSupervisorRepository::class)->update($master);

        $supervisor = new Supervisor(new SupervisorOptions('name', 'redis'));
        resolve(SupervisorRepository::class)->update($supervisor);

        $response = $this->actingAs(new Fakes\User)->get('/horizon/api/masters');

        $response->assertJson([
            gethostname() => [
                'name' => gethostname(),
                'status' => 'paused',
                'supervisors' => [
                    [
                        'name' => 'name',
                        'master' => gethostname(),
                        'status' => 'running',
                        'processes' => ['redis:default' => 0]
                    ],
                ],
            ],
            'risa-2' => [
                'supervisors' => [],
            ],
        ]);
    }
}
