# Horizon Notes

## UI Features

- View Managers & Worker Processes
- View Monitored Tags & Their Results (Including Failures - Put Failures At Top)
- View Failed Jobs By Tag (No list of fail tags... have to know the tag - they only last 2 days)
- View Failed Jobs Chronologically (Paginate from database)

## Data Storage Not Implemented

- View 50 Recently Completed Jobs
- View 50 Recently Completed Broadcast Jobs
- View 50 Recently Completed Event Listener Jobs
- View 50 Recently Completed Mail Jobs
- View 50 Recently Completed Notification Jobs

(We may need to keep separate Redis lists for these things)
(Could be done with a list of hash keys and then the hash can have the queue, connection, etc. information)

## Data Retrieval Methods

- Paginate Pending
- Paginate Recent
- Paginate Completed By Tag
- Paginate Failed
- Paginate Failed By Tag

## Database Usage?

12 - Supervisors
13 - Supervisor Command Queues
14 - Tags
15 - Metrics

## Data Storage

I think we can determine the data types for a job at push time when we set the tags on the payload and can populate the JSON payload with that information then instead of when we unserialize on the user interface side of things.

## Job Type

May be useful to store a type column to contain special job types such as mailables or notifications so we can easily so unique icons for them, etc in the user interface.
