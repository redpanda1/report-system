<?php

use Illuminate\Support\Facades\Route;

// Dashboard Routes...
Route::get('/api/stats', 'DashboardStatsController@index');

// Master Supervisor Routes...
Route::get('/api/masters', 'MasterSupervisorController@index');

// Monitoring Routes...
Route::get('/api/monitoring', 'MonitoringController@index');
Route::post('/api/monitoring', 'MonitoringController@create');
Route::get('/api/monitoring/{tag}/{type?}', 'MonitoringController@paginate');
Route::delete('/api/monitoring/{tag}', 'MonitoringController@destroy');

// Metrics Routes...
Route::get('/api/metrics/jobs', 'MetricsController@jobs');
Route::get('/api/metrics/jobs/{id}', 'MetricsController@job');
Route::get('/api/metrics/queues', 'MetricsController@queues');
Route::get('/api/metrics/queues/{id}', 'MetricsController@queue');

// Jobs Routes...
Route::get('/api/jobs/recent', 'JobsController@recent');
Route::get('/api/jobs/failed', 'JobsController@failedJobs');
Route::get('/api/jobs/failed/{id}', 'JobsController@failedJob');
Route::get('/api/jobs/retry/{id}', 'JobsController@retry');

// App SPA Catch-all Routes
Route::get('/', 'HomeController@index');
Route::get('{view}', 'HomeController@index')->where('view', '(.*)');
