<?php

namespace Laravel\Horizon\Contracts;

interface ProcessRepository
{
    /**
     * Get all of the orphan process IDs and the times they were observed.
     *
     * @return array
     */
    public function allOrphans();

    /**
     * Record the given process IDs as orphaned.
     *
     * @return array
     */
    public function orphaned(array $processIds);

    /**
     * Get the process IDs orphaned for at least the given number of seconds.
     *
     * @param  int  $seconds
     * @return array
     */
    public function orphanedFor($seconds);

    /**
     * Remove the given process IDs from the orphan list.
     *
     * @param  array  $processIds
     * @return void
     */
    public function forgetOrphans(array $processIds);
}
