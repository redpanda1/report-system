<?php

namespace Laravel\Horizon\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Horizon\Contracts\JobRepository;
use Laravel\Horizon\Contracts\TagRepository;
use Laravel\Horizon\Jobs\RetryFailedJob;

class JobsController extends Controller
{
    /**
     * The job repository implementation.
     *
     * @var JobRepository
     */
    private $jobs;

    /**
     * The tag repository implementation.
     *
     * @var TagRepository
     */
    private $tags;

    /**
     * Create a new controller instance.
     *
     * @param  JobRepository $jobs
     * @param  TagRepository $tags
     * @return void
     */
    public function __construct(JobRepository $jobs, TagRepository $tags)
    {
        parent::__construct();

        $this->jobs = $jobs;
        $this->tags = $tags;
    }

    /**
     * Get all of the recent jobs.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function recent(Request $request)
    {
        $jobs = $this->jobs->getRecent($request->query('starting_at', -1))->map(function ($job) {
            $job->payload = json_decode($job->payload);

            return $job;
        });

        return [
            'jobs' => $jobs,
            'total' => $this->jobs->countRecent(),
        ];
    }

    /**
     * Get all of the failed jobs.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function failedJobs(Request $request)
    {
        if ($request->query('tag')) {
            $jobs = $this->jobs->getJobs(
                $this->tags->paginate('failed:'.$request->query('tag'), $request->query('starting_at', - 1) + 1, 50),
                $request->query('starting_at', 0)
            )->map(function ($job) {
                $job->payload = json_decode($job->payload);
                $job->retried_by = json_decode($job->retried_by);

                return $job;
            });
        } else {
            $jobs = $this->jobs->getFailed($request->query('starting_at', - 1))->map(function ($job) {
                $job->payload = json_decode($job->payload);
                $job->retried_by = json_decode($job->retried_by);

                return $job;
            });
        }

        return [
            'jobs' => $jobs,
            'total' => $request->query('tag') ? $this->tags->count('failed:'.$request->query('tag')) : $this->jobs->countFailed(),
        ];
    }

    /**
     * Get a failed job instance.
     *
     * @return \Illuminate\Http\Response
     */
    public function failedJob($id)
    {
        return (array) $this->jobs->getJobs([$id])->map(function ($job) {
            $job->payload = json_decode($job->payload);
            $job->retried_by = json_decode($job->retried_by);

            return $job;
        })->first();
    }

    /**
     * Retry a failed job.
     *
     * @return \Illuminate\Http\Response
     */
    public function retry($id)
    {
        dispatch(new RetryFailedJob($id));
    }
}
