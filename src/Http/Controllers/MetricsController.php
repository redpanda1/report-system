<?php

namespace Laravel\Horizon\Http\Controllers;

use Laravel\Horizon\Contracts\MetricsRepository;

class MetricsController extends Controller
{
    /**
     * The metrics repository implementation.
     *
     * @var MetricsRepository
     */
    private $metrics;

    /**
     * Create a new controller instance.
     *
     * @param  MetricsRepository  $jobs
     * @return void
     */
    public function __construct(MetricsRepository $metrics)
    {
        parent::__construct();

        $this->metrics = $metrics;
    }

    /**
     * Get all of the measured jobs.
     *
     * @return \Illuminate\Http\Response
     */
    public function jobs()
    {
        return $this->metrics->measuredJobs();
    }

    /**
     * Get all of the measured queues.
     *
     * @return \Illuminate\Http\Response
     */
    public function queues()
    {
        return $this->metrics->measuredQueues();
    }

    /**
     * Get metrics for a given job.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function job($slug)
    {
        return collect($this->metrics->snapshotsForJob($slug))->map(function ($record) {
            $record->runtime = ceil($record->runtime / 1000);

            return $record;
        });
    }

    /**
     * Get metrics for a given queue.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function queue($slug)
    {
        return collect($this->metrics->snapshotsForQueue($slug))->map(function ($record) {
            $record->runtime = ceil($record->runtime / 1000);

            return $record;
        });
    }
}
