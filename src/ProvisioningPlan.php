<?php

namespace Laravel\Horizon;

use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Laravel\Horizon\Contracts\HorizonCommandQueue;
use Laravel\Horizon\MasterSupervisorCommands\AddSupervisor;

class ProvisioningPlan
{
    /**
     * The raw provisioning plan.
     *
     * @var array
     */
    public $plan;

    /**
     * The parsed provisioning plan.
     *
     * @var array
     */
    public $parsed;

    /**
     * Create a new provisioning plan instance.
     *
     * @param  array  $plan
     * @return void
     */
    public function __construct(array $plan)
    {
        $this->plan = $plan;

        $this->parsed = $this->toSupervisorOptions();
    }

    /**
     * Get the current provisioning plan.
     *
     * @return static
     */
    public static function get()
    {
        return new static(config('horizon.environments'));
    }

    /**
     * Get all of the defined environments for the provisioning plan.
     *
     * @return array
     */
    public function environments()
    {
        return array_keys($this->parsed);
    }

    /**
     * Determine if the provisioning plan has a given environment.
     *
     * @param  string  $environment
     * @return bool
     */
    public function hasEnvironment($environment)
    {
        return array_key_exists($environment, $this->parsed);
    }

    /**
     * Deploy a provisioning plan to the current machine.
     *
     * @param  string  $environment
     * @return void
     */
    public function deploy($environment)
    {
        $supervisors = collect($this->parsed)->first(function ($_, $name) use ($environment) {
            return Str::is($name, $environment);
        });

        if (empty($supervisors)) {
            return;
        }

        foreach ($supervisors as $supervisor => $options) {
            $this->add($options);
        }
    }

    /**
     * Add a supervisor with the given options.
     *
     * @param  SupervisorOptions  $options
     * @return void
     */
    protected function add(SupervisorOptions $options)
    {
        resolve(HorizonCommandQueue::class)->push(
            MasterSupervisor::commandQueueFor(MasterSupervisor::name()),
            AddSupervisor::class,
            $options->toArray()
        );
    }

    /**
     * Get the SupervisorOptions for a given environment and supervisor.
     *
     * @param  string  $environment
     * @param  string  $supervisor
     * @return SupervisorOptions
     */
    public function optionsFor($environment, $supervisor)
    {
        if (isset($this->parsed[$environment]) && isset($this->parsed[$environment][$supervisor])) {
            return $this->parsed[$environment][$supervisor];
        }
    }

    /**
     * Convert the provisioning plan into an array of SupervisorOptions.
     *
     * @return array
     */
    public function toSupervisorOptions()
    {
        return collect($this->plan)->mapWithKeys(function ($plan, $environment) {
            return [$environment => collect($plan)->mapWithKeys(function ($options, $supervisor) {
                return [$supervisor => $this->convert($supervisor, $options)];
            })];
        })->all();
    }

    /**
     * Convert the given array of options into a SupervisorOptions instance.
     *
     * @param  string  $environment
     * @param  string  $supervisor
     * @param  array  $options
     * @return SupervisorOptions
     */
    protected function convert($supervisor, $options)
    {
        $options = collect($options)->mapWithKeys(function ($value, $key) {
            return [Str::camel($key) => $value];
        })->all();

        return SupervisorOptions::fromArray(
            Arr::add($options, 'name', MasterSupervisor::name().":{$supervisor}")
        );
    }
}
