<?php

namespace Laravel\Horizon;

use StdClass;
use ReflectionClass;
use Illuminate\Mail\SendQueuedMailable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Events\CallQueuedListener;
use Illuminate\Broadcasting\BroadcastEvent;
use Illuminate\Notifications\SendQueuedNotifications;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;

class Tags
{
    /**
     * Determine the tags for the given job.
     *
     * @param  mixed  $job
     * @return array
     */
    public static function for($job)
    {
        return static::modelsFor($job)->map(function ($model) {
            return get_class($model).':'.$model->getKey();
        })->all();
    }

    /**
     * Get the models from the given object.
     *
     * @param  mixed  $target
     * @return \Illuminate\Support\Collection
     */
    public static function modelsFor($target)
    {
        return collect((new ReflectionClass($target))->getProperties())->map(function ($property) use ($target) {
            $property->setAccessible(true);

            $value = $property->getValue($target);

            if ($value instanceof Model) {
                return [$value];
            } elseif ($value instanceof EloquentCollection) {
                return $value->all();
            }
        })->collapse()->filter();
    }

    /**
     * Get the actual target for the given job.
     *
     * @param  mixed  $job
     * @return mixed
     */
    public static function targetFor($job)
    {
        switch (true) {
            case $job instanceof BroadcastEvent:
                return $job->event;
            case $job instanceof CallQueuedListener:
                return static::extractEvent($job);
            case $job instanceof SendQueuedMailable:
                return $job->mailable;
            case $job instanceof SendQueuedNotifications:
                return $job->notification;
            default:
                return $job;
        }
    }

    /**
     * Extract the event from a queued listener job.
     *
     * @param  mixed  $job
     * @return mixed
     */
    protected static function extractEvent($job)
    {
        if (isset($job->data[0]) && is_object($job->data[0])) {
            return $job->data[0];
        }

        return new StdClass;
    }
}
