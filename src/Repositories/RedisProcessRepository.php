<?php

namespace Laravel\Horizon\Repositories;

use Cake\Chronos\Chronos;
use Laravel\Horizon\MasterSupervisor;
use Laravel\Horizon\Contracts\ProcessRepository;
use Illuminate\Contracts\Redis\Factory as RedisFactory;

class RedisProcessRepository implements ProcessRepository
{
    /**
     * The Redis connection instance.
     *
     * @var RedisFactory
     */
    public $redis;

    /**
     * Create a new repository instance.
     *
     * @param  RedisFactory
     * @return void
     */
    public function __construct(RedisFactory $redis)
    {
        $this->redis = $redis;
    }

    /**
     * Get all of the orphan process IDs and the times they were observed.
     *
     * @return array
     */
    public function allOrphans()
    {
        return $this->connection()->hgetall(
            MasterSupervisor::name().':orphans'
        );
    }

    /**
     * Record the given process IDs as orphaned.
     *
     * @return array
     */
    public function orphaned(array $processIds)
    {
        $key = MasterSupervisor::name().':orphans';

        $time = Chronos::now()->getTimestamp();

        $shouldRemove = array_diff($this->connection()->hkeys($key), $processIds);

        if (! empty($shouldRemove)) {
            $this->connection()->hdel($key, ...$shouldRemove);
        }

        $this->connection()->pipeline(function ($pipe) use ($key, $time, $processIds) {
            foreach ($processIds as $processId) {
                $pipe->hsetnx($key, $processId, $time);
            }
        });
    }

    /**
     * Get the process IDs orphaned for at least the given number of seconds.
     *
     * @param  int  $seconds
     * @return array
     */
    public function orphanedFor($seconds)
    {
        $expiresAt = Chronos::now()->getTimestamp() - $seconds;

        return collect($this->allOrphans())->filter(function ($recordedAt, $_) use ($expiresAt) {
            return $expiresAt > $recordedAt;
        })->keys()->all();
    }

    /**
     * Remove the given process IDs from the orphan list.
     *
     * @param  array  $processIds
     * @return void
     */
    public function forgetOrphans(array $processIds)
    {
        $this->connection()->hdel(
            MasterSupervisor::name().':orphans', ...$processIds
        );
    }

    /**
     * Get the Redis connection instance.
     *
     * @return \Illuminate\Redis\Connetions\Connection
     */
    protected function connection()
    {
        return $this->redis->connection('horizon-processes');
    }
}
