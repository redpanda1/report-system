<?php

namespace Laravel\Horizon\Console;

use Illuminate\Console\Command;

class ReviveCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'horizon:revive { --once : Perform a single check that Horizon is running }';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Revive Horizon on the machine if it has died';

    /**
     * Indicates whether the command should be shown in the Artisan command list.
     *
     * @var bool
     */
    protected $hidden = true;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->option('once')) {
            return $this->revive();
        }

        for ($i = 0; $i < 5; $i++) {
            $this->revive();

            sleep(10);
        }
    }

    /**
     * Revive Horizon if necessary.
     *
     * @return void
     */
    protected function revive()
    {
        (new Actions\Revive($this))->handle();
    }
}
