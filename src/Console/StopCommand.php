<?php

namespace Laravel\Horizon\Console;

use Exception;
use Illuminate\Console\Command;
use Laravel\Horizon\MasterSupervisor;
use Laravel\Horizon\SupervisorCommands\Terminate;
use Laravel\Horizon\Contracts\HorizonCommandQueue;
use Laravel\Horizon\Contracts\MasterSupervisorRepository;

class StopCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'horizon:stop';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Terminate Horizon on this machine';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->terminateMasters([MasterSupervisor::name()]);

        $this->ensureTerminated();
    }

    /**
     * Terminate the given master supervisors.
     *
     * @param  array  $supervisors
     * @return void
     */
    protected function terminateMasters(array $masters)
    {
        collect($masters)->each(function ($master) {
            $this->info("Horizon is stopping on [{$master}].");

            resolve(HorizonCommandQueue::class)->push(
                MasterSupervisor::commandQueueFor($master),
                Terminate::class,
                ['status' => 0]
            );
        });
    }

    /**
     * Ensure the given master supervisors are terminated.
     *
     * @return void
     */
    protected function ensureTerminated()
    {
        retry(30, function () {
            $name = MasterSupervisor::name();

            $current = resolve(MasterSupervisorRepository::class)->names();

            if (in_array($name, $current)) {
                throw new Exception("Unable to stop Horizon on [{$name}].");
            }
        }, 1000);
    }
}
