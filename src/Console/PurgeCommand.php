<?php

namespace Laravel\Horizon\Console;

use Illuminate\Console\Command;
use Laravel\Horizon\ProcessInspector;
use Laravel\Horizon\Contracts\ProcessRepository;
use Laravel\Horizon\Contracts\SupervisorRepository;

class PurgeCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'horizon:purge';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Terminate any rogue Horizon processes';

    /**
     * Execute the console command.
     *
     * @param  \Laravel\Horizon\Contracts\SupervisorRepository  $supervisors
     * @param  \Laravel\Horizon\Contracts\ProcessRepository  $processes
     * @param  \Laravel\Horizon\ProcessInspector  $inspector
     * @return mixed
     */
    public function handle(SupervisorRepository $supervisors,
                           ProcessRepository $processes,
                           ProcessInspector $inspector)
    {
        $this->recordOrphans($processes, $inspector);

        $expired = $processes->orphanedFor(
            $supervisors->longestActiveTimeout()
        );

        collect($expired)->each(function ($processId) {
            $this->comment("Killing Process: {$processId}");

            exec("kill {$processId}");

            $processes->forgetOrphans([$processId]);
        });
    }

    /**
     * Record the orphaned Horizon processes.
     *
     * @param  \Laravel\Horizon\Contracts\ProcessRepository  $processes
     * @param  \Laravel\Horizon\ProcessInspector  $inspector
     * @return void
     */
    protected function recordOrphans(ProcessRepository $processes,
                                     ProcessInspector $inspector)
    {
        $processes->orphaned($orphans = $inspector->orphaned());

        foreach ($orphans as $processId) {
            $this->info("Observed Orphan: {$processId}");

            if (function_exists('posix_kill')) {
                posix_kill($processId, SIGTERM);
            }
        }
    }
}
