<?php

namespace Laravel\Horizon\Console;

use Illuminate\Console\Command;
use Laravel\Horizon\MasterSupervisor;
use Laravel\Horizon\Contracts\HorizonCommandQueue;
use Laravel\Horizon\SupervisorCommands\ContinueWorking;
use Laravel\Horizon\Contracts\MasterSupervisorRepository;

class ContinueCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'horizon:continue { machine? : The machine to instruct to continue working }';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Instruct paused machines to continue working';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->argument('machine')) {
            $this->sendContinueCommand([$this->argument('machine')]);
        } else {
            $this->sendContinueCommand(resolve(MasterSupervisorRepository::class)->names());
        }
    }

    /**
     * Send the continue commands to the given masters.
     *
     * @param  array  $masters
     * @return void
     */
    protected function sendContinueCommand(array $masters)
    {
        foreach ($masters as $master) {
            resolve(HorizonCommandQueue::class)->push(
                MasterSupervisor::commandQueueFor($master), ContinueWorking::class, []
            );

            $this->info("[{$master}] has been asked to continue processing jobs.");
        }
    }
}
