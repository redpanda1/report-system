<?php

namespace Laravel\Horizon\Console;

use Illuminate\Console\Command;
use Laravel\Horizon\ProvisioningPlan;

class ProvisionCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'horizon:provision';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Provision the configured supervisors for this machine';

    /**
     * Indicates whether the command should be shown in the Artisan command list.
     *
     * @var bool
     */
    protected $hidden = true;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ProvisioningPlan::get()->deploy(config('app.env'));
    }
}
