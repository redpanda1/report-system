<?php

namespace Laravel\Horizon\Console;

use Illuminate\Console\Command;
use Laravel\Horizon\MasterSupervisor;
use Laravel\Horizon\BackgroundProcess;
use Laravel\Horizon\Contracts\MasterSupervisorRepository;

class StartCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'horizon:start';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Start Horizon in the background';

    /**
     * Indicates whether the command should be shown in the Artisan command list.
     *
     * @var bool
     */
    protected $hidden = true;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->isRunning()) {
            return $this->info('Horizon is already running on this machine.');
        }

        $this->start();

        $this->info('Horizon has started on ['.MasterSupervisor::name().'].');
    }

    /**
     * Start Horizon in the background.
     *
     * @return void
     */
    protected function start()
    {
        ($process = new BackgroundProcess(
            'nohup php artisan horizon > /dev/null &', base_path()
        ))->start();

        while (! $process->isStarted()) {
            usleep(250 * 1000);
        }
    }

    /**
     * Determine if Horizon is already running on this machine.
     *
     * @return bool
     */
    protected function isRunning()
    {
        $repository = resolve(MasterSupervisorRepository::class);

        return ! is_null($repository->find(MasterSupervisor::name()));
    }
}
