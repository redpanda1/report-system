<?php

namespace Laravel\Horizon\Console;

use Illuminate\Console\Command;
use Laravel\Horizon\MasterSupervisor;
use Laravel\Horizon\SupervisorCommands\Pause;
use Laravel\Horizon\Contracts\HorizonCommandQueue;
use Laravel\Horizon\Contracts\MasterSupervisorRepository;

class PauseCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'horizon:pause { machine? : The machine to pause }';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Instruct machines to pause job processing';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->argument('machine')) {
            $this->sendPauseCommand([$this->argument('machine')]);
        } else {
            $this->sendPauseCommand(resolve(MasterSupervisorRepository::class)->names());
        }
    }

    /**
     * Send the pause commands to the given masters.
     *
     * @param  array  $masters
     * @return void
     */
    protected function sendPauseCommand(array $masters)
    {
        foreach ($masters as $master) {
            resolve(HorizonCommandQueue::class)->push(
                MasterSupervisor::commandQueueFor($master), Pause::class, []
            );

            $this->info("[{$master}] has been asked to pause job processing.");
        }
    }
}
