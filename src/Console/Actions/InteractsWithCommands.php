<?php

namespace Laravel\Horizon\Console\Actions;

trait InteractsWithCommands
{
    /**
     * The console command instance.
     *
     * @var \Illuminate\Console\Command
     */
    public $command;

    /**
     * Create a new action instance.
     *
     * @param  \Illuminate\Console\Command  $command
     * @return void
     */
    public function __construct($command)
    {
        $this->command = $command;
    }
}
