<?php

namespace Laravel\Horizon\Console\Actions;

use Laravel\Horizon\Lock;
use Laravel\Horizon\MasterSupervisor;
use Laravel\Horizon\Events\MasterSupervisorReviving;
use Laravel\Horizon\Contracts\MasterSupervisorRepository;

class Revive
{
    use InteractsWithCommands;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        return $this->isRunning() || $this->isDeploying()
                        ? $this->showAlreadyRunningMessage()
                        : $this->revive();
    }

    /**
     * Revive this machine's master supervisor.
     *
     * @return void
     */
    protected function revive()
    {
        // If the master supervisor for this machine is not running, we will revive it by
        // re-deploying it to the machine. We will also fire this event so that any of
        // the listeners can handle it appropriately to perform any additional work.
        event(new MasterSupervisorReviving(
            MasterSupervisor::name()
        ));

        $this->showRevivingMessage();

        $this->command->call('horizon:deploy');
    }

    /**
     * Determine if this machine's master supervisor is running.
     *
     * @return array
     */
    protected function isRunning()
    {
        return in_array(
            MasterSupervisor::name(),
            resolve(MasterSupervisorRepository::class)->names()
        );
    }

    /**
     * Determine if the machine is currently deploying.
     *
     * @return bool
     */
    protected function isDeploying()
    {
        return resolve(Lock::class)->exists(MasterSupervisor::name().':deploying');
    }

    /**
     * Inform the user that the master supervisor is reviving.
     *
     * @return void
     */
    protected function showRevivingMessage()
    {
        $this->command->comment(
            "The [".MasterSupervisor::name()."] machine's master supervisor has died. Restarting."
        );
    }

    /**
     * Inform the user that the master supervisor is already running.
     *
     * @return void
     */
    protected function showAlreadyRunningMessage()
    {
        $this->command->info(
            'Horizon is already running on this machine.'
        );
    }
}
