<?php

namespace Laravel\Horizon\Console\Actions;

use Exception;
use Laravel\Horizon\MasterSupervisor;
use Laravel\Horizon\Events\MasterSupervisorDeployed;
use Laravel\Horizon\Contracts\MasterSupervisorRepository;

class Deploy
{
    use InteractsWithCommands;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // First, we will reprovision the machine, which stops this current master supervisor
        // and starts new ones, including adding their child supervisors. When the machine
        // has been provisioned, we will instruct it to start processing the job queues.
        $this->reprovision();

        $this->continueWorking();

        event(new MasterSupervisorDeployed(MasterSupervisor::name()));

        $this->command->comment(
            'Fresh queue workers deployed to ['.MasterSupervisor::name().'].'
        );
    }

    /**
     * Reprovision this machine's master supervisor.
     *
     * @return void
     */
    protected function reprovision()
    {
        $this->command->call('horizon:stop');

        $this->start();

        $this->command->call('horizon:provision');
    }

    /**
     * Start the master supervisor for this machine.
     *
     * @return void
     */
    protected function start()
    {
        $this->command->call('horizon:start');

        $this->ensureStarted();
    }

    /**
     * Ensure this machine's master supervisor has started.
     *
     * @return void
     */
    protected function ensureStarted()
    {
        retry(10, function () {
            if (! in_array(MasterSupervisor::name(), resolve(MasterSupervisorRepository::class)->names())) {
                throw new Exception("Unable to start master supervisor on this machine.");
            }
        }, 1000);
    }

    /**
     * Instruct the master supervisor to start working if needed.
     *
     * @return void
     */
    protected function continueWorking()
    {
        if (! $this->command->option('wait')) {
            $this->command->call('horizon:continue', ['machine' => MasterSupervisor::name()]);
        }
    }
}
