<?php

namespace Laravel\Horizon\Console;

use Laravel\Horizon\Lock;
use Illuminate\Console\Command;
use Laravel\Horizon\MasterSupervisor;

class DeployCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'horizon:deploy
                                { --wait : Instruct the workers to wait for a "continue" command }';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Provision and deploy the configured queue workers for this machine';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->call('horizon:purge');

        resolve(Lock::class)->with(MasterSupervisor::name().':deploying', function () {
            (new Actions\Deploy($this))->handle();
        });
    }
}
