import Vue from 'vue';
import moment from 'moment';
import router from './router/';
import App from './components/App.vue';

window.Bus = new Vue({name: 'Bus'});

Vue.mixin({
    methods: {
        formatDate(unixTime){
            return moment(unixTime * 1000).add(new Date().getTimezoneOffset() / 60)
        }
    }
});

new Vue({
    el: '#root',


    router,


    /**
     * The component's data.
     */
    data(){
        return {
            showModal: false
        }
    },

    render: h => h(App),
});
